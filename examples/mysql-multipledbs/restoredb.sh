#!/bin/bash

# my_db.sql
echo "Restoring db my_db..."
mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "create database my_db character set utf8mb4 collate utf8mb4_general_ci"
mysql -uroot -p"$MYSQL_ROOT_PASSWORD" my_db < /application/db/my_db.sql
mysql -uroot -p"$MYSQL_ROOT_PASSWORD" -e "grant all privileges on my_db.* to $MYSQL_USER@'%'"
echo "Restoring db my_db done."

echo "Done."
