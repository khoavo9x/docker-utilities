function FindProxyForURL(url, host) {
    if (shExpMatch(host, "*.local")) {
        return "PROXY 192.168.xx.xx:5000";
    }

    return "DIRECT";
}
